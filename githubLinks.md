# Ignite
[React](https://github.com/Daniel-Vinicius/Ignite-React)<br>
[Node](https://github.com/thiagocdn/aulas-Ignite-NodeJS-RocketSeat)<br>
[react-native](https://github.com/pedrocmoreira/rocketseat-ignite-react-native)<br>
[elixir](https://github.com/anajuliabit/ignite-elixir-challenges)<br>
- [elixir](https://github.com/cintiafumi/ignite_fundamentos_elixir)<br>

# GoStack
[GoStack](https://github.com/rocketseat-education/bootcamp-gostack-desafios)<br>

# LaunchBase
[Launchbase](https://github.com/mjulialobo/Launchbase-All-Projects)<br>

# OmniStack
[OmniStack](https://github.com/m7he4rt/Semana-OmniStack)<br>
[OmniStack 2](https://github.com/m7he4rt/Semana-OmniStack-2)<br>
[Omnistack 4](https://github.com/thihxm/Semana-Omnistack-4)<br>
[Omnistack 5](https://github.com/DouglasVarollo/OmniStack5)<br>
[Omnistack 6](https://github.com/DouglasVarollo/OmniStack6)<br>

# GoNativeWeek
[GoNative](https://github.com/patrickchagas/go_native_week)<br>
[GoNative 2](https://github.com/wdmeida/go_native_week)<br>

# Starter
[javascript](https://github.com/douglasabnovato/javascript)<br>
[ecmascript6](https://github.com/douglasabnovato/ecmascript6)<br>
[Node](https://github.com/DouglasVarollo/node-api)<br>
[huntWeb](https://github.com/douglasabnovato/huntweb)<br>
[huntNative](https://github.com/DouglasVarollo/hunt)<br>



# BossaBox
[Backend](https://github.com/Baldessar/BossaBox-Backend)<br>
[Fullstack](https://github-dotcom.gateway.web.tr/flplima/bossabox-vuttr-fullstack)<br>
[Mobile](https://github.com/dbins/bossabox_mobile)<br>
[Frontend](https://github.com/DanielReezende/vuttr-frontend)<br>

# Discover

